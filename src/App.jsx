/** библиотеки */
import React from 'react';

/** компоненты */
import Band from './components/Band/Band';

/** константы */
import { ProgramData } from './store/Program';

/** стили */
import './App.css';

/**
 * Главный компонент приложения
 * @returns
 */
const App = () => {
  /** количество элементов в ленте */
  const ITEMS_FROM_SCREEN = 6;

  /** обработчик выбранного элемента на ленте */
  const onSelect = (item) => {
    // TODO: написать обработчик
  }
  return (
    <div className="app">
      <Band
        list={ProgramData}
        onSelect={onSelect}
        length={ITEMS_FROM_SCREEN}
      />
    </div>
  );
}

export default App;
