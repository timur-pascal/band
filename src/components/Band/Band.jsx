/** библиотеки */
import React, { useEffect, useState } from 'react';

/** компоненты */
import Program from '../Program/Program';

/** утилиты */
import { DoublyLinkedList } from '../../utils/DoublyLinkedList';

/** стили */
import './Band.css';

/**
 * Лента
 * @param {Array} list список элементов
 * @param {number} length количество отображаемых элементов
 * @param {(selectedItem) => {}} onSelect возвращает выбранный элемент
 * @returns
 */
const Band = ({ list, length, onSelect }) => {
  /** двусвязный список элементов */
  const programList = new DoublyLinkedList().fromArray(list);
  /** хранилище выбранного элемента */
  const [selectedItem, setSelectedItem] = useState(programList.head);

  /** устанавливает выбранный элемент ленты и активирует обработчик */
  const selectItem = (item) => {
    setSelectedItem(item);
    onSelect(item);
  }

  /** добавляет обработку нажатия кнопок */
  useEffect(() => {
    document.addEventListener('keydown', (event) => {
      switch (event.key) {
        case 'ArrowRight':
          programList.next();
          selectItem(programList.head);
          break;
        case 'ArrowLeft':
          programList.prev();
          selectItem(programList.head);
          break;
        default:
          break;
      }
    })
  }, [])

  /**
   * Возвращает список для отображения элементов
   * @param {DoublyLinkedList} listItem список элементов
   * @param {number} count длинна нужного списка
   * @returns
   */
  const getViewList = (listItem, count, call) => {
    if (count !== 1) {
      if (listItem[call]) {
        return [listItem, ...getViewList(listItem[call], count - 1, call)];
      } else {
        return [null, ...getViewList(listItem, count - 1, call)];
      }
    } else {
      return [listItem];
    }
  }

  console.log(getViewList(selectedItem, length / 2, 'next'));

  return (
    <div className="band">
      <div className="band__items">
        {selectedItem.previous && getViewList(selectedItem.previous, length / 2, 'previous').reverse().map((item) => {
          return item ? (
            <Program {...item.value} key={item.value._id} />
          ) : '';
        })}
        {getViewList(selectedItem, length / 2, 'next').map((item) => {
          return item ? (
            <Program {...item.value} key={item.value._id} selected={selectedItem === item} />
          ) : '';
        })}
      </div>
    </div>
  );
}

export default Band;
