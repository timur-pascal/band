/** библиотеки */
import React from 'react';

/** стили */
import './Program.css';

/**
 * Блок телепрограммы
 * @param {string} name заголовок телепрограммы
 * @param {Array<string>|string} images список изображений
 * @param {string} description описание
 * @param {boolean} selected статус выбора
 * @returns
 */
const Program = ({ name, images, description, selected }) => {
  /** стили для блока на основе статуса selected */
  const style = `program${selected ? ' program_selected' : ''}`;

  return (
    <div className={style}>
      <div className="program__image">
        <img src={images} alt={name} />
      </div>
      <div className="program__info">
        <h1 className="info__name">{name}</h1>
        <p className="info__description">{description}</p>
      </div>
    </div>
  )
}

export default Program;
